# 🐈‍⬛ README

This is a very tiny Python program I wrote with Code Suggestions.

Here's what the output looks like:

```
😻 ❯ python3 meow.py                                                                                                                                                                          
Here cutie cute
😹 ❯ python3 meow.py                                                                                                                                                                          
The friendliest cat you ever did see
😼 ❯ python3 meow.py                                                                                                                                                                          
Wow look at that super cute cat
😺 ❯ python3 meow.py                                                                                                                                                                        
meow MEOW meow MEOW meow
😸 ❯ python3 meow.py                                                                                                                                                                          
Buy some cute toys for your cute cat
```